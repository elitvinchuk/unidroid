package by.bsuir.egor.classification;

import android.app.AlertDialog;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.LinearLayout;


public class Practical3 extends FragmentActivity implements View.OnClickListener {

    private AlertDialog dialog;
    private LinearLayout buttonsContainer;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_practical3);

        AlertDialog.Builder builder = new AlertDialog.Builder(Practical3.this);
        builder.setMessage(R.string.pr3_user_info)
                .setTitle(R.string.pr3_user_info_button);

        dialog = builder.create();

        buttonsContainer = (LinearLayout) findViewById(R.id.pr3_container);

        findViewById(R.id.pr3_green_button).setOnClickListener(this);
        findViewById(R.id.pr3_red_button).setOnClickListener(this);
        findViewById(R.id.pr3_blue_button).setOnClickListener(this);
        findViewById(R.id.pr3_yellow_button).setOnClickListener(this);
        findViewById(R.id.pr3_user_info_button).setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.pr3_green_button: {
                buttonsContainer.setBackgroundColor(Color.GREEN);
                break;
            }
            case R.id.pr3_red_button: {
                buttonsContainer.setBackgroundColor(Color.RED);
                break;
            }
            case R.id.pr3_blue_button: {
                buttonsContainer.setBackgroundColor(Color.BLUE);
                break;
            }
            case R.id.pr3_yellow_button: {
                buttonsContainer.setBackgroundColor(Color.YELLOW);
                break;
            }
            case R.id.pr3_user_info_button: {
                dialog.show();
                break;
            }
        }
    }
}
