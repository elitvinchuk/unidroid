package by.bsuir.egor.classification;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.EditText;


public class Practical8 extends FragmentActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_practical8);

        final EditText urlField = (EditText) findViewById(R.id.pr8_url);

        Button backBtn = (Button) findViewById(R.id.pr8_button_back);
        Button forwardBtn = (Button) findViewById(R.id.pr8_button_forward);
        Button goBtn = (Button) findViewById(R.id.pr8_button_go);

        final WebView browser = (WebView) findViewById(R.id.pr8_webview);

        goBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url = urlField.getText().toString();

                browser.loadUrl(url);
            }
        });

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (browser.canGoBack()) {
                    browser.goBack();
                }

            }
        });

        forwardBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (browser.canGoForward()) {
                    browser.goForward();
                }
            }
        });
    }
}
