package by.bsuir.egor.classification;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.SeekBar;


public class Practical4 extends FragmentActivity {
    Button syncBtn;
    Button asyncBtn;

    ImageView image1;
    ImageView image2;

    ObjectAnimator animation1;
    ObjectAnimator animation2;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_practical4);

        final SeekBar animationSpeedBar = (SeekBar) findViewById(R.id.pr4_animation_speed);

        syncBtn = (Button) findViewById(R.id.pr4_animate_sync);
        asyncBtn = (Button) findViewById(R.id.pr4_animate_async);

        image1 = (ImageView) findViewById(R.id.pr4_image1);
        image2 = (ImageView) findViewById(R.id.pr4_image2);

        animation1 = ObjectAnimator.ofFloat(image1, "rotation", 0f, 360f);
        animation2 = ObjectAnimator.ofFloat(image2, "rotation", 0f, 360f);

        syncBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int animationSpeed = animationSpeedBar.getProgress() + 500;

                animation1.setDuration(animationSpeed);
                animation2.setDuration(animationSpeed);

                AnimatorSet set = new AnimatorSet();
                set.playTogether(animation1, animation2);
                set.start();
            }
        });

        asyncBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int animationSpeed = animationSpeedBar.getProgress() + 500;

                animation1.setDuration(animationSpeed);
                animation2.setDuration(animationSpeed);

                animation1.start();



                animation1.addListener(new AnimatorListenerAdapter() {

                    @Override
                    public void onAnimationEnd(Animator animation) {
                        animation2.start();

                        animation1.removeListener(this);
                    }
                });
            }
        });
    }
}

