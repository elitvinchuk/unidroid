package by.bsuir.egor.classification.practical5;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Gallery;
import android.widget.ImageView;

/**
 * Created by egor on 3/10/17.
 */

public class GalleryImageAdapter extends BaseAdapter {

    private Context mContext;

    public GalleryImageAdapter(Context context) {
        mContext = context;
    }

    public int getCount() {
        return mImageIds.length;
    }

    public Object getItem(int position) {
        return position;
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(int index, View view, ViewGroup viewGroup) {
        ImageView i = new ImageView(mContext);

        i.setImageResource(mImageIds[index]);
        i.setLayoutParams(new Gallery.LayoutParams(200, 200));
        i.setScaleType(ImageView.ScaleType.FIT_XY);

        return i;
    }

    public Integer[] mImageIds = {
            /*R.drawable.alarm,
            R.drawable.explore,
            R.drawable.language,
            R.drawable.lock,
            R.drawable.print,
            R.drawable.rotation_3d,
            R.drawable.spellcheck,
            R.drawable.redeem*/
    };
}
