package by.bsuir.egor.classification;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.widget.CheckedTextView;


public class Practical7 extends FragmentActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_practical7);

        CheckedTextView cameraPermission = (CheckedTextView) findViewById(R.id.pr7_permission_camera);
        cameraPermission.setChecked(ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED);

        CheckedTextView contactsReadPermission = (CheckedTextView) findViewById(R.id.pr7_permission_contacts_read);
        contactsReadPermission.setChecked(ContextCompat.checkSelfPermission(this, Manifest.permission.READ_CONTACTS) == PackageManager.PERMISSION_GRANTED);

        CheckedTextView contactsWritePermission = (CheckedTextView) findViewById(R.id.pr7_permission_contacts_write);
        contactsWritePermission.setChecked(ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_CONTACTS) == PackageManager.PERMISSION_GRANTED);

        CheckedTextView wifiPermission = (CheckedTextView) findViewById(R.id.pr7_permission_wifi);
        wifiPermission.setChecked(ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_WIFI_STATE) == PackageManager.PERMISSION_GRANTED);

        CheckedTextView batteryPermission = (CheckedTextView) findViewById(R.id.pr7_permission_battery);
        batteryPermission.setChecked(ContextCompat.checkSelfPermission(this, Manifest.permission.BATTERY_STATS) == PackageManager.PERMISSION_GRANTED);

    }
}
