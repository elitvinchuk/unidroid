package by.bsuir.egor.classification.practical6;


import android.util.Log;

import java.io.*;

public class FileOperations {
    public FileOperations() {

    }

    public Boolean write(String name, String content) {
        try {
            String path = "/sdcard/" + name + ".txt";

            File file = new File(path);

            if (!file.exists()) {
                file.createNewFile();
            }

            FileWriter writer = new FileWriter(file.getAbsoluteFile());
            BufferedWriter buffered = new BufferedWriter(writer);
            buffered.write(content);
            buffered.close();

            Log.d("Success", "Success");
            return true;

        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    public String read(String name) {
        BufferedReader reader;
        String content;

        try {
            StringBuffer output = new StringBuffer();
            String path = "/sdcard/" + name + ".txt";

            reader = new BufferedReader(new FileReader(path));
            String line;

            while ((line = reader.readLine()) != null) {
                output.append(line + "n");
            }

            content = output.toString();

        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }

        return content;
    }
}
