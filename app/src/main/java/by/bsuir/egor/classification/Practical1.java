package by.bsuir.egor.classification;

import android.app.AlertDialog;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;

public class Practical1 extends FragmentActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_practical1);

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.pr1_dialog_message)
                .setTitle(R.string.pr1_dialog_title);

        final AlertDialog dialog = builder.create();

        findViewById(R.id.pr1_button)
                .setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.show();
                    }
                });
    }
}
