package by.bsuir.egor.classification;

import android.app.Fragment;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import by.bsuir.egor.classification.practical6.FileOperations;
import org.json.JSONException;
import org.json.JSONObject;


public class Practical6 extends FragmentActivity {

    EditText searchQuery;
    Button searchBtn;

    EditText nameFld;
    EditText groupFld;
    EditText facultyFld;
    Button submitBtn;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_practical6);

        final FileOperations operations = new FileOperations();

        searchQuery = (EditText) findViewById(R.id.pr6_search_query);
        searchBtn = (Button) findViewById(R.id.pr6_search_button);

        searchBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String studentQuery = searchQuery.getText().toString();
                String rawContent = operations.read(studentQuery);

                if (rawContent != null) {
                    JSONObject student;

                    try {
                        student = new JSONObject(rawContent);

                        nameFld.setText(student.get("name").toString());
                        groupFld.setText(student.get("group").toString());
                        facultyFld.setText(student.get("faculty").toString());

                    } catch (JSONException e) {
                        e.printStackTrace();

                        Toast
                                .makeText(getApplicationContext(), "Ошибка файла", Toast.LENGTH_SHORT)
                                .show();
                    }
                } else {
                    nameFld.setText(null);
                    groupFld.setText(null);
                    facultyFld.setText(null);

                    Toast
                            .makeText(getApplicationContext(), "Студент не найден", Toast.LENGTH_SHORT)
                            .show();
                }
            }
        });

        nameFld = (EditText) findViewById(R.id.pr6_name);
        groupFld = (EditText) findViewById(R.id.pr6_group);
        facultyFld = (EditText) findViewById(R.id.pr6_faculty);
        submitBtn = (Button) findViewById(R.id.pr6_submit_button);

        submitBtn.setOnClickListener((new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String studentQuery = searchQuery.getText().toString();
                String studentName = nameFld.getText().toString();

                JSONObject student = new JSONObject();
                try {
                    student.put("name", studentName);
                    student.put("group", groupFld.getText().toString());
                    student.put("faculty", facultyFld.getText().toString());
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if (studentQuery.length() > 0 && operations.write(studentQuery, student.toString())) {
                    Toast
                            .makeText(getApplicationContext(),
                                    "Студент " + studentName + " добавлен/обновлён",
                                    Toast.LENGTH_SHORT)
                            .show();
                } else {
                    Toast
                            .makeText(getApplicationContext(), "Ошибка", Toast.LENGTH_SHORT)
                            .show();
                }
            }
        }));
    }
}
