package by.bsuir.egor.classification;

import android.app.ActionBar;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;


public class MainActivity extends FragmentActivity implements View.OnClickListener {

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final ActionBar actionBar = getActionBar();
        actionBar.setHomeButtonEnabled(false);

        // todo: optimize
        findViewById(R.id.button1).setOnClickListener(this);
        findViewById(R.id.button2).setOnClickListener(this);
        findViewById(R.id.button3).setOnClickListener(this);
        findViewById(R.id.button4).setOnClickListener(this);
        findViewById(R.id.button5).setOnClickListener(this);
        findViewById(R.id.button6).setOnClickListener(this);
        findViewById(R.id.button7).setOnClickListener(this);
        findViewById(R.id.button8).setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        Intent intent;

        // todo: optimize
        switch (view.getId()) {
            case R.id.button1: {
                intent = new Intent(MainActivity.this, Practical1.class);
                break;
            }
            case R.id.button2: {
                intent = new Intent(MainActivity.this, Practical2.class);
                break;
            }
            case R.id.button3: {
                intent = new Intent(MainActivity.this, Practical3.class);
                break;
            }
            case R.id.button4: {
                intent = new Intent(MainActivity.this, Practical4.class);
                break;
            }
            case R.id.button5: {
                intent = new Intent(MainActivity.this, Practical5.class);
                break;
            }
            case R.id.button6: {
                intent = new Intent(MainActivity.this, Practical6.class);
                break;
            }
            case R.id.button7: {
                intent = new Intent(MainActivity.this, Practical7.class);
                break;
            }
            case R.id.button8: {
                intent = new Intent(MainActivity.this, Practical8.class);
                break;
            }
            default: {
                intent = new Intent(MainActivity.this, Practical8.class);
            }
        }

        startActivity(intent);
    }
}
